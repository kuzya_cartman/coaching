<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Крыму и Севастополе</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр1111"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Забайкальском крае</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра Крымского ФО:</p>
		  
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Родник"</span><br />

			   <span class="locality"><strong>Республика Крым, Сакский район, г. Евпатория</strong></span>   

			   <span class="street-address">Межквартальный пр-д, д.6</span>

			   <div>Телефон: <span class="tel">+380656957439,  +380676537771</span></div></div>

			</div>
			
			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, Белогорский район, г. Белогорск</strong></span>   

			   <span class="street-address">ул. Индустриальная, д. 2</span>

			   <div>Телефон: <span class="tel">+380507732777</span></div></div>

			</div>
			
			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, Красногвардейский район, пгт Красногвардейское</strong></span>   

			   <span class="street-address">ул. Полевая, д. 1</span>

			   <div>Телефон: <span class="tel">+30668463862</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Симферополь</strong></span>   

			   <span class="street-address">ул. Объездная, д. 6</span>

			   <div>Телефон: <span class="tel">+380503603144</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Джанкой</strong></span>   

			   <span class="street-address">ул. Толстого, д. 64</span>

			   <div>Телефон: <span class="tel">+380667400894</span></div></div>

			</div>
			
			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Евпатория</strong></span>   

			   <span class="street-address">ш. Раздольненское, д. 21</span>

			   <div>Телефон: <span class="tel">+380506371298</span></div></div>

			</div>
			
			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Красноперекопск</strong></span>   

			   <span class="street-address">ул. 2-я Промышленная, д. 2</span>

			   <div>Телефон: <span class="tel">+380506313241</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ООО "Компания АТК"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Ялта</strong></span>   

			   <span class="street-address">ул. Большевистская, д. 22</span>

			   <div>Телефон: <span class="tel">+380990138634</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Симферопольский участок КРЦАТИ "Крымавтотест"</span><br />

			   <span class="locality"><strong>Республика Крым, Симферопольский район</strong></span>   

			   <span class="street-address">с. Дубки, ул. Гаражная, д. 6</span>

			   <div>Телефон: <span class="tel">+380652668858</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ЧП "КЕРЧЕНСКОЕ АТП"</span><br />

			   <span class="locality"><strong>Республиука Крым, г. Керчь</strong></span>   

			   <span class="street-address">ш. Вокзальное, д.44 к. 7</span>

			   <div>Телефон: <span class="tel">+38065656190204, +38065656120315</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ООО "Тест-АВТО"</span><br />

			   <span class="locality"><strong>Республиука Крым, г. Севастополь</strong></span>   

			   <span class="street-address">ул. Промышленная, д. 4</span>

			   <div>Телефон: <span class="tel">+380504242014</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ЧП "АвтоСпецЦентр им. Э.Ф.Хаваджи"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Симферополь</strong></span>   

			   <span class="street-address">ул. Мамеди Эмир Усеина, д. 14</span>

			   <div>Телефон: <span class="tel">+380652548454</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Осадчий Руслан Олегович</span><br />

			   <span class="locality"><strong>Республика Крым, г. Керчь</strong></span>   

			   <span class="street-address">ул. Маршала Еременко, д. 11</span>

			   <div>Телефон: <span class="tel">+380656561527</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ЧП "Авторитет М"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Симферополь</strong></span>   

			   <span class="street-address">ул. Киевская, д. 187</span>

			   <div>Телефон: <span class="tel">+380652545455</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Кочкоров Эрнес Яяевич</span><br />

			   <span class="locality"><strong>Республика Крым, г. Бахчисарай</strong></span>   

			   <span class="street-address">ул. Мира, д. 11</span>

			   <div>Телефон: <span class="tel">+380957861528, +380666596664</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ООО "Престиж-Авто"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Севастополь</strong></span>   

			   <span class="street-address">ул. Руднева, д. 35Г</span>

			   <div>Телефон: <span class="tel">+380692555700, +380692554477</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ООО "СВК"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Ялта</strong></span>   

			   <span class="street-address">ул. Халтурина, д. 52</span>

			   <div>Телефон: <span class="tel">+380654273708</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ЧММП "ДЮН"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Джанкой</strong></span>   

			   <span class="street-address">ул. Московская, д. 188</span>

			   <div>Телефон: <span class="tel">+380656431275, +380999011766</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Кривцова-Богатырева Виктория Витальевна</span><br />

			   <span class="locality"><strong>Республика Крым, г. Феодосия</strong></span>   

			   <span class="street-address">ул. Еременко, д. 24А</span>

			   <div>Телефон: <span class="tel">+380656291004</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ЧП "СДЦ КТС"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Севастополь</strong></span>   

			   <span class="street-address">ул. Паршина, д. 6-А</span>

			   <div>Телефон: <span class="tel">+380692482941</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Бахчисарайское РайПО</span><br />

			   <span class="locality"><strong>Республика Крым, г. Бахчисарай</strong></span>   

			   <span class="street-address">ул. Кооперативная, д. 6</span>

			   <div>Телефон: <span class="tel">+380655441960</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ООО "СБ-СЕРВИС"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Керчь</strong></span>   

			   <span class="street-address">ул. Энгельса, д. 6</span>

			   <div>Телефон: <span class="tel">+380656167582</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ПАО "ССТС"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Симферополь</strong></span>   

			   <span class="street-address">ул. Монтажная, д. 8</span>

			   <div>Телефон: <span class="tel">+380 652618918</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">ООО "Неаполь-Авто"</span><br />

			   <span class="locality"><strong>Республика Крым, г. Симферополь</strong></span>   

			   <span class="street-address">пер. Элеваторный, д. 10А</span>

			   <div>Телефон: <span class="tel">+380504974010</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Частное Предприятие "ТЕХНОПОЛИС"</span><br />

			   <span class="locality"><strong>Республика Крым, пгт Нижняя Кутузовка</strong></span>   

			   <span class="street-address">ул Ленина, д.116</span>

			   <div>Телефон: <span class="tel">+38099709702</span></div></div>

			</div>
			
			<div class="vcard">
			
			<div class="adr">

				<span class="fn org">Лопаткин Александр Петрович</span><br />

			   <span class="locality"><strong>Республика Крым, пгт Красногвардейское</strong></span>   

			   <span class="street-address">ул 50 лет Октября, д.26</span>

			   <div>Телефон: <span class="tel">+380655623884</span></div></div>

			</div>
			
			<div class="vcard">

			<div class="adr">

				<span class="fn org">Топунов Павел Борисович</span><br />

			   <span class="locality"><strong>Республика Крым, г Феодосия</strong></span>   

			   <span class="street-address">ул 3-го Интернационала, д. 32</span>

			   <div>Телефон: <span class="tel">+380505803727</span></div></div>
            <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>