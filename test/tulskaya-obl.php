<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Тульской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Тульской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Тульская область, адреса, телефоны"/>
<meta name="classification" content="transportationweqwe"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Тульской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Тульской области:</p>
			
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Дорбезопасность"</span><br />

			   <span class="locality"><strong>Тульская область, Ленинский район</strong></span>   

			   <span class="street-address">192 км а/д "Крым" </span>

			   <div>Телефон: <span class="tel">(4872) 219525</span></div><div class="email">dorbez@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Дорбезопасность"</span><br />

			   <span class="locality"><strong>301212, Тульская обл, Щекинский р-н, рп Первомайский</strong></span>   

			   <span class="street-address">ул Административная, 17В </span>

			   <div>Телефон: <span class="tel">(48751) 64014</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Дорбезопасность"</span><br />

			   <span class="locality"><strong>301430, Тульская обл, Суворовский р-н, г Суворов</strong></span>   

			   <span class="street-address">ул Островского, 7 </span>

			   <div>Телефон: <span class="tel">(48763) 23198, 20098</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Дорбезопасность"</span><br />

			   <span class="locality"><strong>301830, Тульская обл, Богородицкий р-н, г Богородицк</strong></span>   

			   <span class="street-address">ул Ф.Энгельса, 24 </span>

			   <div>Телефон: <span class="tel">(48761) 24167</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Туладорбезопасность"</span><br />

			   <span class="locality"><strong>301130, Тульская обл, Ленинский р-н, рп Ленинский</strong></span>   

			   <span class="street-address">ул Циолковского, 92 </span>

			   <div>Телефон: <span class="tel">(48767) 92464</span></div><div class="email">dorbezopas1@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Туладорбезопасность"</span><br />

			   <span class="locality"><strong>Тульская обл, Ефремовский р-н, г Ефремов</strong></span>   

			   <span class="street-address">ул Тульское шоссе, 1 </span>

			   <div>Телефон: <span class="tel">(48741) 61797, 68101</span></div><div class="email">dorbezopas1@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Туладорбезопасность"</span><br />

			   <span class="locality"><strong>301657, Тульская обл, Новомосковский р-н, г Новомосковск</strong></span>   

			   <span class="street-address">ул Техническая, 8 </span>

			   <div>Телефон: <span class="tel">(48762) 32314,</span></div><div class="email">dorbezopas1@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Туладорбезопасность"</span><br />

			   <span class="locality"><strong>301602, Тульская обл, Узловский р-н, г Узловая</strong></span>   

			   <span class="street-address">ул Дубовская, 6 </span>

			   <div>Телефон: <span class="tel">(48731) 57077</span></div><div class="email">dorbezopas1@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Туладорбезопасность"</span><br />

			   <span class="locality"><strong>301087, Тульская обл, Чернский р-н, п Скуратовский (сп Большескуратовское)</strong></span>   

			   <span class="street-address">ул А.Скуратова </span>

			   <div>Телефон: <span class="tel">(48756) 36131</span></div><div class="email">dorbezopas1@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Одоевское"</span><br />

			   <span class="locality"><strong>301440, Тульская обл, Одоевский р-н, рп Одоев</strong></span>   

			   <span class="street-address">ул Победы, 53 А </span>

			   <div>Телефон: <span class="tel">(48736) 41599</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Технопромсервис"</span><br />

			   <span class="locality"><strong>301770, Тульская обл, г Донской, мкр Новоугольный</strong></span>   

			   <span class="street-address">ул Заводская, 33 А </span>

			   <div>Телефон: <span class="tel">(48746) 32509</span></div><div class="email">tpskusov@yandex.ru, kusovtps@rambler.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Киреевский Центр Технического Контроля"</span><br />

			   <span class="locality"><strong>301260, Тульская обл, Киреевский р-н, г Киреевск</strong></span>   

			   <span class="street-address">ул Геологов, 26 </span>

			   <div>Телефон: <span class="tel">(48748) 67452</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Трансконтроль"</span><br />

			   <span class="locality"><strong>301361, Тульская обл, Алексинский р-н, г Алексин</strong></span>   

			   <span class="street-address">ул Матросова, 3 </span>

			   <div>Телефон: <span class="tel">(910)9440394</span></div><div class="email">ooo.transkontrol@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техническая диагностика"</span><br />

			   <span class="locality"><strong>301940, Тульская обл, Куркинский р-н, рп Куркино</strong></span>   

			   <span class="street-address">ул Октябрьская, 139А </span>

			   <div>Телефон: <span class="tel">(48743)41100 42420</span></div><div class="email">tdiagnostika@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Куприянов Владимир Валерьевич</span><br />

			   <span class="locality"><strong>301900, Тульская обл, Тепло-Огаревский р-н, рп Теплое</strong></span>   

			   <span class="street-address">ул Привокзальная, 17 </span>

			   <div>Телефон: <span class="tel">(48755) 21-967</span></div><div class="email">vladimir@teploe.tula.net</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Белуха Ирина Александровна</span><br />

			   <span class="locality"><strong>301722, Тульская обл, Кимовский р-н, г Кимовск</strong></span>   

			   <span class="street-address">ул Пушкина, 1 </span>

			   <div>Телефон: <span class="tel">(48735) 41-701</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Уткин Николай Николаевич</span><br />

			   <span class="locality"><strong>301510, Тульская обл, Арсеньевский р-н, рп Арсеньево</strong></span>   

			   <span class="street-address">ул Хорева, 24В </span>

			   <div>Телефон: <span class="tel">8(48733)52-8-42</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Винюков Павел Николаевич</span><br />

			   <span class="locality"><strong>301723, Тульская обл, Кимовский р-н, г Кимовск</strong></span>   

			   <span class="street-address">ул Коммунистическая, 3В </span>

			   <div>Телефон: <span class="tel">(48735)50623</span></div><div class="email">VINYUKOVPTO@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Ревметавто"</span><br />

			   <span class="locality"><strong>301032, Тульская обл, Ясногорский р-н, г Ясногорск</strong></span>   

			   <span class="street-address">ул Заводская, 3 </span>

			   <div>Телефон: <span class="tel">(920) 7459000</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Транс-Сервис"</span><br />

			   <span class="locality"><strong>301000, Тульская обл, Заокский р-н, рп Заокский</strong></span>   

			   <span class="street-address">ул Шоссейная, 2 </span>

			   <div>Телефон: <span class="tel">(48734) 20-161</span></div><div class="email">sitnikov.trans-servis@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Петухова Светлана Николаевна</span><br />

			   <span class="locality"><strong>301990, Тульская обл, Каменский р-н, с Архангельское</strong></span>   

			   <span class="street-address">ул Гагарина </span>

			   <div>Телефон: <span class="tel">(48744) 21160</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Гаврилюк Дмитрий Николаевич</span><br />

			   <span class="locality"><strong>301160, Тульская обл, Дубенский р-н, п Дубна</strong></span>   

			   <span class="street-address">ул Свободы, 19А </span>

			   <div>Телефон: <span class="tel">(920)7653752</span></div><div class="email">Tehosmotrdubna@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ЦТК"</span><br />

			   <span class="locality"><strong>301831, Тульская обл, Богородицкий р-н, г Богородицк</strong></span>   

			   <span class="street-address">ул Октябрьская, 34 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Суворовавтосервис</span><br />

			   <span class="locality"><strong>301430, Тульская обл, Суворовский р-н, г Суворов</strong></span>   

			   <span class="street-address">ул Островского, 7 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Чернский ТЦ БДД"</span><br />

			   <span class="locality"><strong>301090, Тульская обл, Чернский р-н, рп Чернь</strong></span>   

			   <span class="street-address">ул Свободная, 114 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Иванов Вадим Павлович</span><br />

			   <span class="locality"><strong>301530, Тульская обл, Белевский р-н, г Белев</strong></span>   

			   <span class="street-address">ул Советская, 1 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "СТК-АВТО"</span><br />

			   <span class="locality"><strong>301320, Тульская обл, Веневский р-н, г Венев</strong></span>   

			   <span class="street-address">ул Кольцевая, 1 корп.А </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Чистякова Татьяна Дмитриевна</span><br />

			   <span class="locality"><strong>301320, Тульская обл, Веневский р-н, г Венев</strong></span>   

			   <span class="street-address">ул Кольцевая, 26 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
			   <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>


          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	  
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
	  
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>