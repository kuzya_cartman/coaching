<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Тюменской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Тюменской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Тюменская область, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Тюменской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Тюменской области:</p>
			
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Промсервис"</span><br />

			   <span class="locality"><strong>627570, Тюменская обл, Викуловский р-н, с Викулово</strong></span>   

			   <span class="street-address">ул Свободы, 158, стр.1 </span>

			   <div>Телефон: <span class="tel">(34557) 24062</span></div><div class="email">vk-vikylovo@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Диагност"</span><br />

			   <span class="locality"><strong>627070, Тюменская обл, Омутинский р-н, с Омутинское</strong></span>   

			   <span class="street-address">ул Строителей, 2А </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Черный Виктор Иванович</span><br />

			   <span class="locality"><strong>626022, Тюменская обл, Нижнетавдинский р-н, с Нижняя Тавда</strong></span>   

			   <span class="street-address">ул Тюменская, 57 </span>

			   <div>Телефон: <span class="tel">(904) 4973132</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Усольцев Михаил Петрович</span><br />

			   <span class="locality"><strong>627540, Тюменская обл, Абатский р-н, с Абатское</strong></span>   

			   <span class="street-address"> ул Береговая, 33 </span>

			   <div>Телефон: <span class="tel">(34556) 52090</span></div><div class="email">usolcevmp@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Платонов Андрей Андреевич</span><br />

			   <span class="locality"><strong>627350, Тюменская обл, Аромашевский р-н, с Аромашево</strong></span>   

			   <span class="street-address">ул Луговая, 80 </span>

			   <div>Телефон: <span class="tel">(34545) 22560</span></div><div class="email">fokus070.ru@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Анафин Ташат Айдарханович</span><br />

			   <span class="locality"><strong>627420, Тюменская обл, Казанский р-н, с Казанское</strong></span>   

			   <span class="street-address">ул Ишимская, 48В </span>

			   <div>Телефон: <span class="tel">(34553) 43062</span></div><div class="email">800st@bk.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ПО "Тобол"</span><br />

			   <span class="locality"><strong>627180, Тюменская обл, Упоровский р-н, с Упорово</strong></span>   

			   <span class="street-address">ул Молодежная, 2 </span>

			   <div>Телефон: <span class="tel">8 (34541) 32247</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Извин Максим Владимирович</span><br />

			   <span class="locality"><strong>Тюменская область, Уватский район, 2-ой км подъездной дороги  на п. Туртас</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">(34561)25813</span></div><div class="email">maxim_200364@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Попова Елена Владимировна</span><br />

			   <span class="locality"><strong>626050, Тюменская обл, Ярковский р-н, с Ярково</strong></span>   

			   <span class="street-address">ул Аэродромная, 13 </span>

			   <div>Телефон: <span class="tel">(34531)25133</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Ишимтехконтроль"</span><br />

			   <span class="locality"><strong>627750, Тюменская обл, Ишимский р-н, г Ишим</strong></span>   

			   <span class="street-address">ул Республики, 99А </span>

			   <div>Телефон: <span class="tel">(34551) 62192</span></div><div class="email">ishimtechcontrol@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автотех"</span><br />

			   <span class="locality"><strong>625501, Тюменская обл, Тюменский р-н, п Московский</strong></span>   

			   <span class="street-address">ул Бурлаки, 25 </span>

			   <div>Телефон: <span class="tel">89088742510</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Мясникова Лариса Евгеньевна</span><br />

			   <span class="locality"><strong>627017, Тюменская обл, Ялуторовский р-н, г Ялуторовск</strong></span>   

			   <span class="street-address">ул Л.Чайкиной, 48 </span>

			   <div>Телефон: <span class="tel">(34535) 22609</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автомастер"</span><br />

			   <span class="locality"><strong>626150, Тюменская обл, г Тобольск</strong></span>   

			   <span class="street-address">ул Семена Ремезова, 115 </span>

			   <div>Телефон: <span class="tel">(3456) 258992, 253274, 241781</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Эврика"</span><br />

			   <span class="locality"><strong>Тюменская область, г.Тобольск, мкр.Менделеево</strong></span>   

			   <span class="street-address">ул.Комсомольская, д.5 </span>

			   <div>Телефон: <span class="tel">(3456)362445</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Столяр Алексей Васильевич</span><br />

			   <span class="locality"><strong>Тюменская область, Голышмановский район</strong></span>   

			   <span class="street-address">215 км+950 метр федеральной автомобильной дороги «Тюмень-Ишим-Омск». </span>

			   <div>Телефон: <span class="tel">(34546)28029</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ДМК"</span><br />

			   <span class="locality"><strong>Тюменская область, г.Ишим</strong></span>   

			   <span class="street-address">ул.Республики, д.4 </span>

			   <div>Телефон: <span class="tel">(34551)58996</span></div><div class="email">dmk.07@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Куклин Александр Иванович</span><br />

			   <span class="locality"><strong>627013, Тюменская обл, Ялуторовский р-н, г Ялуторовск</strong></span>   

			   <span class="street-address">ул Механизаторов, д.34 </span>

			   <div>Телефон: <span class="tel">(345) 3523726</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ОАО "Дорсервисуниверсал"</span><br />

			   <span class="locality"><strong>627143, Тюменская обл, Заводоуковский р-н, г Заводоуковск</strong></span>   

			   <span class="street-address">ул Революционная, 94 А </span>

			   <div>Телефон: <span class="tel">(345)4261210</span></div><div class="email">dorcervis@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техконтроль"</span><br />

			   <span class="locality"><strong>627610, Тюменская обл, Сладковский р-н, с Сладково</strong></span>   

			   <span class="street-address">ул Ленина, 88 </span>

			   <div>Телефон: <span class="tel">(34555) 23731</span></div><div class="email">tavolyan@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АвтоДиагност"</span><br />

			   <span class="locality"><strong>626150, Тюменская обл, г Тобольск</strong></span>   

			   <span class="street-address">ул Семена Ремезова, 89 </span>

			   <div>Телефон: <span class="tel">(3456) 251296, 249002</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">НОУ "Тобольская автошкола ВОА"</span><br />

			   <span class="locality"><strong>626150, Тюменская обл, г Тобольск</strong></span>   

			   <span class="street-address">ул Семена Ремезова, 101 А </span>

			   <div>Телефон: <span class="tel">(345)6250803</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Бертрам Елена Сергеевна</span><br />

			   <span class="locality"><strong>627140, Тюменская обл, Заводоуковский р-н, г Заводоуковск</strong></span>   

			   <span class="street-address">ул Шоссейная, 171 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Тобольсксервискомплект"</span><br />

			   <span class="locality"><strong>626156, Тюменская обл, г Тобольск</strong></span>   

			   <span class="street-address">ул Сельская, 1 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
			   <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>


          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	  
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
	  
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>