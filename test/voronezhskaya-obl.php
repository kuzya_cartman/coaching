<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Воронежской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Воронежской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Воронежская область, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Воронежской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Воронежской области:</p>
		  
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Автогарант"</span><br />

			   <span class="locality"><strong>396311, Воронежская обл, Новоусманский р-н, с Новая Усмань</strong></span>   

			   <span class="street-address">ул Полевая, 48а</span>

			   <div>Телефон: <span class="tel">(47341) 24560</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автотехконтроль"</span><br />

			   <span class="locality"><strong>397110, Воронежская обл, Терновский р-н, с Терновка</strong></span>   

			   <span class="street-address">ул Усадьба СХТ, Площадка</span>

			   <div>Телефон: <span class="tel">(909)2125746</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техкон"</span><br />

			   <span class="locality"><strong>396657, Воронежская обл, Россошанский р-н, г Россошь</strong></span>   

			   <span class="street-address">ул Промышленная, 6</span>

			   <div>Телефон: <span class="tel">(910)7444386</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техстандарт"</span><br />

			   <span class="locality"><strong>397030, Воронежская обл, Эртильский р-н, г Эртиль</strong></span>   

			   <span class="street-address">ул Лесная, 1Б</span>

			   <div>Телефон: <span class="tel">(47345)24368</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техкон-Авто"</span><br />

			   <span class="locality"><strong>397601, Воронежская обл, Калачеевский р-н, г Калач</strong></span>   

			   <span class="street-address">ул Верхнезаводская, 9</span>

			   <div>Телефон: <span class="tel">(47363)24607</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техкон-Авто"</span><br />

			   <span class="locality"><strong>397902, Воронежская обл, Лискинский р-н, г Лиски</strong></span>   

			   <span class="street-address">ул 40 лет Октября, 70Б</span>

			   <div>Телефон: <span class="tel">(47391)32536</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Диагностика"</span><br />

			   <span class="locality"><strong>396902, Воронежская обл, Семилукский р-н, г Семилуки</strong></span>   

			   <span class="street-address">ул Курская, 109</span>

			   <div>Телефон: <span class="tel">(47372)23839</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Пантера-Аннинская"</span><br />

			   <span class="locality"><strong>396254, Воронежская обл, Аннинский р-н, пгт Анна</strong></span>   

			   <span class="street-address">ул Энгельса, 40А</span>

			   <div>Телефон: <span class="tel">(47346) 27260</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Олейников Виталий Александрович</span><br />

			   <span class="locality"><strong>396462, Воронежская обл, Верхнемамонский р-н, с Верхний Мамон</strong></span>   

			   <span class="street-address">ул Прогресс, 21</span>

			   <div>Телефон: <span class="tel">89204289444; (47355) 41898</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ЛК-Транс"</span><br />

			   <span class="locality"><strong>396731, Воронежская обл, Кантемировский р-н, рп Кантемировка</strong></span>   

			   <span class="street-address">ул Ленина, 348</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техносервис"</span><br />

			   <span class="locality"><strong>397702, Воронежская обл, Бобровский р-н, г Бобров</strong></span>   

			   <span class="street-address">ул 22 Января, 2</span>

			   <div>Телефон: <span class="tel">(47350) 47254</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто-Сервис"</span><br />

			   <span class="locality"><strong>397350, Воронежская обл, Поворинский р-н, г Поворино</strong></span>   

			   <span class="street-address">ул Олимпийская, 2</span>

			   <div>Телефон: <span class="tel">(47376) 44313</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Романенко Василий Васильевич</span><br />

			   <span class="locality"><strong>396370, Воронежская обл, Репьевский р-н, с Репьевка</strong></span>   

			   <span class="street-address">ул Воронежская, 71</span>

			   <div>Телефон: <span class="tel">(47374) 22748</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ТЕХКОНТРОЛЬ"</span><br />

			   <span class="locality"><strong>397160, Воронежская обл, Борисоглебский р-н, г Борисоглебск</strong></span>   

			   <span class="street-address">ул Матросовская, 162</span>

			   <div>Телефон: <span class="tel">(47354) 66253; 66269</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Ремтехника"</span><br />

			   <span class="locality"><strong>396560, Воронежская обл, Подгоренский р-н, пгт Подгоренский</strong></span>   

			   <span class="street-address">ул Ленина, 1</span>

			   <div>Телефон: <span class="tel">(47394) 54973</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автопарус"</span><br />

			   <span class="locality"><strong>396510, Воронежская обл, Каменский р-н, пгт Каменка</strong></span>   

			   <span class="street-address">ул Солнечная, 2</span>

			   <div>Телефон: <span class="tel">(47357) 51357</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автотест"</span><br />

			   <span class="locality"><strong>397853, Воронежская обл, Острогожский р-н, г Острогожск</strong></span>   

			   <span class="street-address">ул Карла Маркса, 57Б</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Бинкон"</span><br />

			   <span class="locality"><strong>397505, Воронежская обл, Бутурлиновский р-н, г Бутурлиновка</strong></span>   

			   <span class="street-address">ул Дорожная, 73</span>

			   <div>Телефон: <span class="tel">(47361) 33676</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автодиагностика"</span><br />

			   <span class="locality"><strong>397481, Воронежская обл, Таловский р-н, рп Таловая</strong></span>   

			   <span class="street-address">ул Клишина, 1А</span>

			   <div>Телефон: <span class="tel">(47352) 21015</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Озон плюс"</span><br />

			   <span class="locality"><strong>397441, Воронежская обл, Новохоперский р-н, рп Новохоперский</strong></span>   

			   <span class="street-address">ул Пушкина, 21</span>

			   <div>Телефон: <span class="tel">(47353) 35328</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Ишкова Татьяна Геннадьевна</span><br />

			   <span class="locality"><strong>396070, Воронежская обл, г Нововоронеж</strong></span>   

			   <span class="street-address">ул Вокзальная, 18</span>

			   <div>Телефон: <span class="tel">(47364)25100; 50900; 53542</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Ишкова Татьяна Геннадьевна</span><br />

			   <span class="locality"><strong>396350, Воронежская обл, Каширский р-н, с Каширское</strong></span>   

			   <span class="street-address">ул Мира, 41 А</span>

			   <div>Телефон: <span class="tel">89204036597</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автодиагност"</span><br />

			   <span class="locality"><strong>396110, Воронежская обл, Верхнехавский р-н, с Верхняя Хава</strong></span>   

			   <span class="street-address">ул Железнодорожная, 132 А</span>

			   <div>Телефон: <span class="tel">8(960)1022000; (47343)21759</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ИнКон плюс"</span><br />

			   <span class="locality"><strong>396020, Воронежская обл, Рамонский р-н, рп Рамонь</strong></span>   

			   <span class="street-address">ул Рабочая, 11</span>

			   <div>Телефон: <span class="tel">(47340)23476</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техконтроль"</span><br />

			   <span class="locality"><strong>396140, Воронежская обл, Панинский р-н, рп Панино</strong></span>   

			   <span class="street-address">ул Юбилейная, 12 А</span>

			   <div>Телефон: <span class="tel">89515517411</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авторесурс"</span><br />

			   <span class="locality"><strong>396420, Воронежская обл, Павловский р-н, г Павловск</strong></span>   

			   <span class="street-address">ул Транспортная, 4</span>

			   <div>Телефон: <span class="tel">(473) 2689391; (47362)31148</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Фаворит"</span><br />

			   <span class="locality"><strong>396691, Воронежская обл, Ольховатский р-н, п Заболотовка</strong></span>   

			   <span class="street-address">ул Новаторов, 14 К</span>

			   <div>Телефон: <span class="tel">(473295) 31595</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АвтоТехЦентр"</span><br />

			   <span class="locality"><strong>397691, Воронежская обл, Петропавловский р-н, с Красноселовка</strong></span>   

			   <span class="street-address">ул Южная, 32</span>

			   <div>Телефон: <span class="tel">(47365) 21111; 22150</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Замчалов Андрей Викторович</span><br />

			   <span class="locality"><strong>396784, Воронежская обл, Богучарский р-н, с Залиман</strong></span>   

			   <span class="street-address">ул Малаховского, 211Е</span>

			   <div>Телефон: <span class="tel">(47366)21225</span></div><div class="email">ZAV-PTO@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Качкин Николай Васильевич</span><br />

			   <span class="locality"><strong>397240, Воронежская обл, Грибановский р-н, пгт Грибановский</strong></span>   

			   <span class="street-address">ул Лесная, 3</span>

			   <div>Телефон: <span class="tel">(47348) 36000</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автосервис"</span><br />

			   <span class="locality"><strong>397571, Воронежская обл, Воробьевский р-н, с Воробьевка</strong></span>   

			   <span class="street-address">ул Пушкинская, 35</span>

			   <div>Телефон: <span class="tel">(47356) 52304; 31355</span></div></div>
            <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>