<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Тверской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Тверской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Тверская область, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Тверской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Тверской области:</p>
			
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Такма"</span><br />

			   <span class="locality"><strong>171251, Тверская обл, Конаковский р-н, г Конаково</strong></span>   

			   <span class="street-address">ул Белавинская, 46 </span>

			   <div>Телефон: <span class="tel">(48242) 35819</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "СДЛ-Сервис"</span><br />

			   <span class="locality"><strong>172734, Тверская обл, Осташковский р-н, г Осташков</strong></span>   

			   <span class="street-address">ул Урожайная, 9 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Быстров Леонид Леонидович</span><br />

			   <span class="locality"><strong>172840, Тверская обл, Торопецкий р-н, г Торопец</strong></span>   

			   <span class="street-address">ул Рябиновая, 17 </span>

			   <div>Телефон: <span class="tel">(48268) 21201</span></div><div class="email">Diagnostika69@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авторесурс"</span><br />

			   <span class="locality"><strong>172527, Тверская обл, г Нелидово</strong></span>   

			   <span class="street-address">ул Советская, 57 А </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Альтернатива"</span><br />

			   <span class="locality"><strong>171252, Тверская обл, Конаковский р-н, г Конаково</strong></span>   

			   <span class="street-address">ул Учебная, 14 </span>

			   <div>Телефон: <span class="tel">8 (910) 932-30-67</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техносервис-ПТК"</span><br />

			   <span class="locality"><strong>171843, Тверская обл, Удомельский р-н, г Удомля</strong></span>   

			   <span class="street-address">ул Пионерская </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Мухамеджанова Оксана Владимировна</span><br />

			   <span class="locality"><strong>172400, Тверская обл, Оленинский р-н, пгт Оленино</strong></span>   

			   <span class="street-address">ул Гагарина, 65 </span>

			   <div>Телефон: <span class="tel">(903)8056767</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Мобиль"</span><br />

			   <span class="locality"><strong>171161, Тверская обл, г Вышний Волочек</strong></span>   

			   <span class="street-address">ш Московское, 294 км </span>

			   <div>Телефон: <span class="tel">(48233)53055</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Тарасов Сергей Сергеевич</span><br />

			   <span class="locality"><strong>171841, Тверская обл, Удомельский р-н, г Удомля</strong></span>   

			   <span class="street-address">ул Тверская, 7 </span>

			   <div>Телефон: <span class="tel">(4825)554579</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Универсал Плюс"</span><br />

			   <span class="locality"><strong>Тверская обл, г Кимры</strong></span>   

			   <span class="street-address">ул 50 лет ВЛКСМ </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто-норма"</span><br />

			   <span class="locality"><strong>171161, Тверская обл, г Вышний Волочек</strong></span>   

			   <span class="street-address">ш Московское, 101 </span>

			   <div>Телефон: <span class="tel">(48233) 61608</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Евролайн"</span><br />

			   <span class="locality"><strong>171640, Тверская обл, Кашинский р-н, г Кашин</strong></span>   

			   <span class="street-address">ул Строителей, 7 </span>

			   <div>Телефон: <span class="tel">(910) 6480317</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Евролайн"</span><br />

			   <span class="locality"><strong>172530, Тверская обл, Бельский р-н, г Белый</strong></span>   

			   <span class="street-address">ул Солнечная, 5 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВД.К"</span><br />

			   <span class="locality"><strong>171573, Тверская обл, Калязинский р-н, г Калязин</strong></span>   

			   <span class="street-address">ул Заводская, 17 </span>

			   <div>Телефон: <span class="tel">4824925417</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Торжок-Транс"</span><br />

			   <span class="locality"><strong>172007, Тверская обл, г Торжок</strong></span>   

			   <span class="street-address">ш Калининское, 47 </span>

			   <div>Телефон: <span class="tel">(482) 5192165</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Беляков Геннадий Васильевич</span><br />

			   <span class="locality"><strong>171900, Тверская обл, Максатихинский р-н, пгт Максатиха</strong></span>   

			   <span class="street-address">ул Бежецкая, 67 </span>

			   <div>Телефон: <span class="tel">4825321995</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО СТД"</span><br />

			   <span class="locality"><strong>171080, Тверская обл, Бологовский р-н, г Бологое</strong></span>   

			   <span class="street-address">ул Куженкинское шоссе, 42 корп.а </span>

			   <div>Телефон: <span class="tel">(48238) 22229</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Мастер Групп"</span><br />

			   <span class="locality"><strong>171982, Тверская обл, Бежецкий р-н, г Бежецк</strong></span>   

			   <span class="street-address">ул Тверская, 32 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Монолит"</span><br />

			   <span class="locality"><strong>172381, Тверская обл, г Ржев</strong></span>   

			   <span class="street-address">ул Декабристов, 63 </span>

			   <div>Телефон: <span class="tel">(910) 6482450</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Надежда"</span><br />

			   <span class="locality"><strong>172332, Тверская обл, Зубцовский р-н, г Зубцов</strong></span>   

			   <span class="street-address">ул Московская Гора, 22 </span>

			   <div>Телефон: <span class="tel">8 (960) 7057565</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Лагуна"</span><br />

			   <span class="locality"><strong>г. Селижарово </strong></span>   

			   <span class="street-address">ул. Ленина, д. 113б </span>

			   <div>Телефон: <span class="tel">8 (906) 727-38-56</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ПО "КУДД Тверской области"</span><br />

			   <span class="locality"><strong>172381, Тверская обл, г Ржев</strong></span>   

			   <span class="street-address">ул Декабристов, 55 </span>

			   <div>Телефон: <span class="tel">(910) 9324982</span></div><div class="email">ohota@rzev.tver.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "КВАНТ+"</span><br />

			   <span class="locality"><strong>171660, Тверская обл, Краснохолмский р-н, г Красный Холм</strong></span>   

			   <span class="street-address">ул Базарная, 54 </span>

			   <div>Телефон: <span class="tel">4832722135</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто-Тест"</span><br />

			   <span class="locality"><strong>Тверская обл.,Андреапольский район, Андреапольское сельское поселение, д.Обруб</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">(910) 937-53-83</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "Сервис +"</span><br />

			   <span class="locality"><strong>172610, Тверская обл, Западнодвинский р-н, г Западная Двина</strong></span>   

			   <span class="street-address">ул Калинина, 5 </span>

			   <div>Телефон: <span class="tel">(48265)21206</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ЦЕНТР-СЕРВИС-СОЛЕКС"</span><br />

			   <span class="locality"><strong></strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">8-904-350-91-57</span></div><div class="email">en.kacekin@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Беляков Виктор Алексеевич</span><br />

			   <span class="locality"><strong>Тверская обл, г Кимры</strong></span>   

			   <span class="street-address">ул Урицкого, 90 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Амтэк+"</span><br />

			   <span class="locality"><strong>171720, Тверская обл, Весьегонский р-н, г Весьегонск </strong></span>   

			   <span class="street-address">ул Серова, 58а </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
			   <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	  
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
	  
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>