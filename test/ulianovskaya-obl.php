<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Ульяновской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Ульяновской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Ульяновская область, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Ульяновской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Ульяновской области:</p>
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Волготранссервис"</span><br />

			   <span class="locality"><strong>433400, Ульяновская обл, Чердаклинский р-н, рп Чердаклы</strong></span>   

			   <span class="street-address">ул Советская, 144 </span>

			   <div>Телефон: <span class="tel">88422981998</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Волготранссервис"</span><br />

			   <span class="locality"><strong>433380, Ульяновская обл, Сенгилеевский р-н, г Сенгилей</strong></span>   

			   <span class="street-address">ул Котовского, 44 </span>

			   <div>Телефон: <span class="tel">88422981998</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Волготранссервис"</span><br />

			   <span class="locality"><strong>433810, Ульяновская обл, Николаевский р-н, рп Николаевка</strong></span>   

			   <span class="street-address">ул Железнодорожная, 1 </span>

			   <div>Телефон: <span class="tel">88422981998</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Волготранссервис"</span><br />

			   <span class="locality"><strong>433360, Ульяновская обл, Тереньгульский р-н, рп Тереньга</strong></span>   

			   <span class="street-address">ул Куйбышева, 52 </span>

			   <div>Телефон: <span class="tel">88422981998</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Котельников Петр Аркадьевич</span><br />

			   <span class="locality"><strong>Ульяновская обл., р.п. Б. Нагаткино</strong></span>   

			   <span class="street-address">ул. Молочкова, 37 </span>

			   <div>Телефон: <span class="tel">88422731517</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Никишечкина Зинаида Николаевна</span><br />

			   <span class="locality"><strong>433870, Ульяновская обл, Новоспасский р-н, рп Новоспасское</strong></span>   

			   <span class="street-address">ул Заводская, 7 </span>

			   <div>Телефон: <span class="tel">(927) 6346316</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Мотор"</span><br />

			   <span class="locality"><strong>433760, Ульяновская обл, Кузоватовский р-н, рп Кузоватово</strong></span>   

			   <span class="street-address">ул Калинина, 3А </span>

			   <div>Телефон: <span class="tel">89276341557</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">УРООО "ВОА"</span><br />

			   <span class="locality"><strong>433750, Ульяновская обл, г Барыш</strong></span>   

			   <span class="street-address">ул Тростинского, 3А </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">УРООО "ВОА"</span><br />

			   <span class="locality"><strong>433970, Ульяновская обл, Павловский р-н, рп Павловка</strong></span>   

			   <span class="street-address">ул Партизанская, 23 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто-СПАС"</span><br />

			   <span class="locality"><strong>433130, Ульяновская обл, Майнский р-н, рп Майна</strong></span>   

			   <span class="street-address">ул Пионерская, 37 </span>

			   <div>Телефон: <span class="tel">(842) 2464149</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто-СПАС"</span><br />

			   <span class="locality"><strong>433240, Ульяновская обл, Сурский р-н, рп Сурское</strong></span>   

			   <span class="street-address">ул Хазова, 133 </span>

			   <div>Телефон: <span class="tel">(842) 2724601</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Хабиев Фярид Имадиевич</span><br />

			   <span class="locality"><strong>433940, Ульяновская обл, Старокулаткинский р-н, рп Старая Кулатка</strong></span>   

			   <span class="street-address">ул Пугачева, 102 </span>

			   <div>Телефон: <span class="tel">(8842)4921196 (8927)9830600</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ТрансЭксперт"</span><br />

			   <span class="locality"><strong>433750, Ульяновская обл, г Барыш</strong></span>   

			   <span class="street-address">ул Радищева, 90 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АКС"</span><br />

			   <span class="locality"><strong>433504, Ульяновская обл, г Димитровград</strong></span>   

			   <span class="street-address">ул Промышленная, 26 </span>

			   <div>Телефон: <span class="tel">(927) 8135835</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО УКЦ "Ульяновскавтотранс"</span><br />

			   <span class="locality"><strong>Ульяновская обл., г. Новоульяновск</strong></span>   

			   <span class="street-address">ул. Ульяновская, д. 24 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Конти"</span><br />

			   <span class="locality"><strong>433460, Ульяновская обл, Старомайнский р-н, рп Старая Майна</strong></span>   

			   <span class="street-address">ул Сидорова, 56 </span>

			   <div>Телефон: <span class="tel">. 88422981998</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто-Хим"</span><br />

			   <span class="locality"><strong>433310, Ульяновская обл, Ульяновский р-н, рп Ишеевка</strong></span>   

			   <span class="street-address">ул Новокомбинатовская, 6 В </span>

			   <div>Телефон: <span class="tel">( 884229)91564</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ПТО №17"</span><br />

			   <span class="locality"><strong>Ульяновская обл, г Димитровград</strong></span>   

			   <span class="street-address">ул Куйбышева, 30 </span>

			   <div>Телефон: <span class="tel">(927)2720859</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автотест"</span><br />

			   <span class="locality"><strong>433034, Ульяновская обл, Инзенский р-н, г Инза</strong></span>   

			   <span class="street-address">ул Шоссейная, 92 </span>

			   <div>Телефон: <span class="tel">8(902)1238840</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Стожаров Александр Николаевич</span><br />

			   <span class="locality"><strong>433100, Ульяновская обл, Вешкаймский р-н, рп Вешкайма</strong></span>   

			   <span class="street-address">ул Железнодорожная, 2 Б </span>

			   <div>Телефон: <span class="tel">тел. (927)6321841</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Асфендиярова Марьям Абдулловна</span><br />

			   <span class="locality"><strong>433910, Ульяновская обл, Радищевский р-н, рп Радищево</strong></span>   

			   <span class="street-address">ул Кооперативная, 27 </span>

			   <div>Телефон: <span class="tel">9278073681</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Чернов Владимир Викторович</span><br />

			   <span class="locality"><strong>433210, Ульяновская обл, Карсунский р-н, рп Карсун</strong></span>   

			   <span class="street-address">ул Курдюмова, 9 </span>

			   <div>Телефон: <span class="tel">89041869727</span></div></div>
			   <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	  
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
	  
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>