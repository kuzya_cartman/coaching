<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Ямало-ненецком Автономном округе</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Ямало-ненецком Автономном округе. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Ямало-ненецкий Автономный округ, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Ямало-ненецком Автономном округе</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Ямало-ненецком Автономном округе:</p>
			
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Газпром добыча Ямбург"</span><br />

			   <span class="locality"><strong>Ямало-ненецкий автономный округ, Тюменская обл., Надымский р-он, п.Ямбург</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">8(34949)-6-78-40, 6-78-25</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ТМУДТП</span><br />

			   <span class="locality"><strong>629350, Ямало-Ненецкий АО, Тазовский р-н, пгт Тазовский</strong></span>   

			   <span class="street-address">ул Дорожная, стр.1 </span>

			   <div>Телефон: <span class="tel">(34940)-2-22-36; (34940)-2-21-92</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Газпром трансгаз Югорск"</span><br />

			   <span class="locality"><strong>629730, Ямало-Ненецкий АО, г Надым</strong></span>   

			   <span class="street-address">ул Ю.Топчева, база УТТиСТ </span>

			   <div>Телефон: <span class="tel">8(34995)-4-96-57</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техцентр"</span><br />

			   <span class="locality"><strong>Ямало-Ненецкий автономный округ, г. Новый Уренгой</strong></span>   

			   <span class="street-address">западная промзона, панель "В" </span>

			   <div>Телефон: <span class="tel">(3494) 974943</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Тройка-Сервис"</span><br />

			   <span class="locality"><strong>629003, Ямало-Ненецкий АО, г Салехард</strong></span>   

			   <span class="street-address">ул Ангальский Мыс, Промбаза </span>

			   <div>Телефон: <span class="tel">8(34922) 4-82-01, 4-85-88</span></div><div class="email">sto-trojka@mail.ru; trojkaservice@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МУП "АТП"</span><br />

			   <span class="locality"><strong>629730, Ямало-Ненецкий АО, г Надым</strong></span>   

			   <span class="street-address">проезд 8-й </span>

			   <div>Телефон: <span class="tel">8(34995)- 2-29-67</span></div><div class="email">Mup-natp@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Газпром добыча уренгой"</span><br />

			   <span class="locality"><strong>629307, Ямало-Ненецкий АО, г Новый Уренгой</strong></span>   

			   <span class="street-address">ул Железнодорожная, 6 </span>

			   <div>Телефон: <span class="tel">(34949)48111</span></div><div class="email">lisichnikova@gdurengoy.gazprom.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Газпром добыча Надым"</span><br />

			   <span class="locality"><strong>629730, Ямало-Ненецкий АО, г Надым</strong></span>   

			   <span class="street-address">ул Заводская, панель М </span>

			   <div>Телефон: <span class="tel">8(34995)- 6-42-50</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Газпром добыча Надым"</span><br />

			   <span class="locality"><strong>629757, Ямало-Ненецкий АО, Надымский р-н, пгт Пангоды</strong></span>   

			   <span class="street-address">проезд Медвежье, 5 </span>

			   <div>Телефон: <span class="tel">8(34995)- 6-44-45, 6-42-54</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Рыбалкин Игорь Анатольевич</span><br />

			   <span class="locality"><strong>629800, Ямало-Ненецкий АО, г Ноябрьск</strong></span>   

			   <span class="street-address">тер Панель 3 </span>

			   <div>Телефон: <span class="tel">(3496) 343082, 354675</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Мегаполис"</span><br />

			   <span class="locality"><strong>г.Тарко-Сале</strong></span>   

			   <span class="street-address">промзона СУМВР, административное здание №7 </span>

			   <div>Телефон: <span class="tel">(34997)26025</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МУП "Автодорсервис" г. Губкинский</span><br />

			   <span class="locality"><strong>Ямало-Ненецкий автономный округ, г.Губкинский</strong></span>   

			   <span class="street-address">Промзона панель №2 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Диагностика"</span><br />

			   <span class="locality"><strong>г.Муравленко</strong></span>   

			   <span class="street-address">промзона, панель №6 </span>

			   <div>Телефон: <span class="tel">(34938) 32300</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "ЗСК"</span><br />

			   <span class="locality"><strong>Пуровский район</strong></span>   

			   <span class="street-address">промбаза филиала "Газурстрой", 1-й км. автодороги г.Новый Уренгой-Коротчаево </span>

			   <div>Телефон: <span class="tel">8(3494) 939043, 939042</span></div><div class="email">Zck@list.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Думанищев Эстасий Мухарбович</span><br />

			   <span class="locality"><strong>629400, Ямало-Ненецкий АО, г Лабытнанги</strong></span>   

			   <span class="street-address">ул Овражная, 2 </span>

			   <div>Телефон: <span class="tel">8(34992)-2-36-89, 2-34-93</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МУП "СпецТрансСервис"</span><br />

			   <span class="locality"><strong>629640, Ямало-Ненецкий АО, Шурышкарский р-н, с Мужи</strong></span>   

			   <span class="street-address">ул Рыбацкая, 28 </span>

			   <div>Телефон: <span class="tel">(34994) 21872, (908) 8626582,</span></div><div class="email">mvji7S@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АКС"</span><br />

			   <span class="locality"><strong>Надымский район, п.Пангоды</strong></span>   

			   <span class="street-address">промбаза  ОАО "АНГС" </span>

			   <div>Телефон: <span class="tel">(34995 59583, 57437, 90096</span></div><div class="email">aksdiligans@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "Трест Ямалстройгазодобыча"</span><br />

			   <span class="locality"><strong>Ямальский район</strong></span>   

			   <span class="street-address">НГКМ Бованенково </span>

			   <div>Телефон: <span class="tel">(332)402685, (332)401663</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Захаров Александр Васильевич</span><br />

			   <span class="locality"><strong>629840, Ямало-Ненецкий АО, Пуровский р-н, п Пурпе</strong></span>   

			   <span class="street-address">ул Школьная, 56 </span>

			   <div>Телефон: <span class="tel">(922)2873107, (349)03667183</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Козюбенко Иголь Иванович</span><br />

			   <span class="locality"><strong>629800, Ямало-Ненецкий АО, г Ноябрьск</strong></span>   

			   <span class="street-address">тер Панель 3 </span>

			   <div>Телефон: <span class="tel">8-922-051-8173, 8-902-824-0175</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автодор"</span><br />

			   <span class="locality"><strong>629380, Ямало-Ненецкий АО, Красноселькупский р-н, с Красноселькуп</strong></span>   

			   <span class="street-address">тер Промышленная зона </span>

			   <div>Телефон: <span class="tel">(34932) 22129</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "ТАС"</span><br />

			   <span class="locality"><strong>629730, Ямало-Ненецкий АО, г Надым</strong></span>   

			   <span class="street-address">ул 107 км, промбаза </span>

			   <div>Телефон: <span class="tel">(34995) 90071, 90069, 38733</span></div><div class="email">tas.gorbik@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Сервистрансстрой"</span><br />

			   <span class="locality"><strong>629305, Ямало-Ненецкий АО, г Новый Уренгой</strong></span>   

			   <span class="street-address">ул Магистральная, 2 </span>

			   <div>Телефон: <span class="tel">(3494)939047   (3494)939127</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Сервистрансстрой"</span><br />

			   <span class="locality"><strong>Передвижной Пункт Технического Осмотра</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">(3494)939047   (3494)939127</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "МТК"</span><br />

			   <span class="locality"><strong>ЯНАО, г. Муравленко</strong></span>   

			   <span class="street-address">промзона панель №10, ПТТ-1 </span>

			   <div>Телефон: <span class="tel">(4938)65222</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Гвоздарев Юрий Васильевич</span><br />

			   <span class="locality"><strong>629800, Ямало-Ненецкий АО, г Ноябрьск</strong></span>   

			   <span class="street-address">тер Промузел Пелей Панель 7 </span>

			   <div>Телефон: <span class="tel">(3496)343048</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ЦИК"</span><br />

			   <span class="locality"><strong>629601, г Муравленко</strong></span>   

			   <span class="street-address">ул Энергетиков, панель, корп 3 </span>

			   <div>Телефон: <span class="tel">(34938)-6-58-72, 8-922-056-55-55</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МУПАТ</span><br />

			   <span class="locality"><strong>629307, Ямало-Ненецкий АО, г Новый Уренгой</strong></span>   

			   <span class="street-address">пр-кт Губкина, 10 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МП "ЯТП"</span><br />

			   <span class="locality"><strong>629700, Ямало-Ненецкий АО, Ямальский р-н, с Яр-Сале</strong></span>   

			   <span class="street-address">ул Советская, 54/3 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
			   <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	  
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
	  
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>