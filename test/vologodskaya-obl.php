<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Вологодской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Вологодской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Вологодская область, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Вологодской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Вологодской области:</p>
		  
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО АК "Регион"</span><br />

			   <span class="locality"><strong>Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ш Северное, 10</span>

			   <div>Телефон: <span class="tel">(8202) 633535</span></div><div class="email">vau35@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Сельхозтехника"</span><br />

			   <span class="locality"><strong>Вологодская область, Шекснинский район</strong></span>   

			   <span class="street-address">п.Подгорный, д.43</span>

			   <div>Телефон: <span class="tel">(81751)42165</span></div><div class="email">oao-cxt@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Коркка Сергей Таунович</span><br />

			   <span class="locality"><strong>Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ш Северное, 6в</span>

			   <div>Телефон: <span class="tel">(8202) 310109</span></div><div class="email">kst-61@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Макурина Нина Александровна</span><br />

			   <span class="locality"><strong>162300, Вологодская обл, Верховажский р-н, с Верховажье</strong></span>   

			   <span class="street-address">ул Тендрякова, 36</span>

			   <div>Телефон: <span class="tel">(81759)21270</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Болотуев Антон Александрович</span><br />

			   <span class="locality"><strong>162614, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Добролюбова, 1</span>

			   <div>Телефон: <span class="tel">(8202)544485</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МУП "Автоколонна №1456"</span><br />

			   <span class="locality"><strong>162603, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Боршодская, 12</span>

			   <div>Телефон: <span class="tel">(8202) 284537</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АТП-1"</span><br />

			   <span class="locality"><strong>162130, Вологодская обл, Сокольский р-н, г Сокол</strong></span>   

			   <span class="street-address">пер Станционный, 31</span>

			   <div>Телефон: <span class="tel">(81733) 338-38</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Волпромсервис"</span><br />

			   <span class="locality"><strong>Вологодская область, г.Тотьма</strong></span>   

			   <span class="street-address">ул.Запольная, д.17А</span>

			   <div>Телефон: <span class="tel">(81739) 21874</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Истомин Игорь Вячеславович</span><br />

			   <span class="locality"><strong>162604, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ш Кирилловское, 49</span>

			   <div>Телефон: <span class="tel">(8202) 291857</span></div><div class="email">istominiv@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Кудрявцев Андрей Анатольевич</span><br />

			   <span class="locality"><strong>162601, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">пр-кт Победы, 10А</span>

			   <div>Телефон: <span class="tel">(8202) 57-22-79</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Баранов Сергей Евгеньевич</span><br />

			   <span class="locality"><strong>Вологодская область, Череповецкий р-н </strong></span>   

			   <span class="street-address">ТСГ "Шексна" корпус №48, бокс №10</span>

			   <div>Телефон: <span class="tel">(8202) 620237</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Лесторг"</span><br />

			   <span class="locality"><strong>162250, Вологодская обл, Харовский р-н, г Харовск</strong></span>   

			   <span class="street-address">ул Архангельская, 42</span>

			   <div>Телефон: <span class="tel">(81732) 21774</span></div><div class="email">valmir@vologda.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Транзит"</span><br />

			   <span class="locality"><strong>162677, Вологодская обл, Череповецкий р-н, пгт Тоншалово</strong></span>   

			   <span class="street-address">ул Мелиораторов, 3</span>

			   <div>Телефон: <span class="tel">9215355634</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Кадуйавтотранс"</span><br />

			   <span class="locality"><strong>162510, Вологодская обл, Кадуйский р-н, пгт Кадуй</strong></span>   

			   <span class="street-address">ул Промышленная, 2</span>

			   <div>Телефон: <span class="tel">(81742) 522-44</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Андреевский Сергей Витальевич</span><br />

			   <span class="locality"><strong>161409, Вологодская обл, Кичменгско-Городецкий р-н, с Кичменгский Городок</strong></span>   

			   <span class="street-address">ул Заречная, 63</span>

			   <div>Телефон: <span class="tel">(81740) 22196</span></div><div class="email">yugleskg@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ОАО "Северсталь"</span><br />

			   <span class="locality"><strong>162611, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Устюженская, 97</span>

			   <div>Телефон: <span class="tel">(8202) 57-04-28</span></div><div class="email">adermolin@severstal.com</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">НТЦ ДАТ ЧГУ</span><br />

			   <span class="locality"><strong>162602, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">пр-кт Советский, 25</span>

			   <div>Телефон: <span class="tel">(8202) 51-81-27</span></div><div class="email">chsu@chsu.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Энергия"</span><br />

			   <span class="locality"><strong>162510, Вологодская обл, Кадуйский р-н, пгт Кадуй</strong></span>   

			   <span class="street-address">ул Энтузиастов, 1Б</span>

			   <div>Телефон: <span class="tel">(81742) 520-10</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автомобильная компания"</span><br />

			   <span class="locality"><strong>162603, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Краснодонцев, 3</span>

			   <div>Телефон: <span class="tel">(8202)238564</span></div><div class="email">anzh75mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Такси-Сервис Плюс"</span><br />

			   <span class="locality"><strong>162603, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Гоголя, 43</span>

			   <div>Телефон: <span class="tel">(8202)244843</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "Реммашсервис"</span><br />

			   <span class="locality"><strong>162900, Вологодская обл, Вытегорский р-н, г Вытегра</strong></span>   

			   <span class="street-address">ул Заводская дорога, 1</span>

			   <div>Телефон: <span class="tel">(81746) 2-22-46</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Бильчугов Алексей Николаевич</span><br />

			   <span class="locality"><strong>162160, Вологодская обл, Вожегодский р-н, п Вожега</strong></span>   

			   <span class="street-address">ул Октябрьская, 42а</span>

			   <div>Телефон: <span class="tel">(817) 4421412</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Хайвей"</span><br />

			   <span class="locality"><strong>162626, Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Рыбинская</span>

			   <div>Телефон: <span class="tel">(8202) 601813</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "СтройВариант"</span><br />

			   <span class="locality"><strong>Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Белинского, 1/3</span>

			   <div>Телефон: <span class="tel">(8202) 244082</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Устюг Авто"</span><br />

			   <span class="locality"><strong>162390, Вологодская обл, Великоустюгский р-н, г Великий Устюг</strong></span>   

			   <span class="street-address">ул Транспортная, 4</span>

			   <div>Телефон: <span class="tel">(81738) 21158</span></div><div class="email">aleks-35-us@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АвтотрансМаркет"</span><br />

			   <span class="locality"><strong>Вологодская обл, г Череповец</strong></span>   

			   <span class="street-address">ул Краснодонцев, 1</span>

			   <div>Телефон: <span class="tel">(8202) 21-65-40</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Русинов Александр Вениаминович</span><br />

			   <span class="locality"><strong>161560, Вологодская обл, Тарногский р-н, с Тарногский Городок</strong></span>   

			   <span class="street-address">ул Пролетарская, 29</span>

			   <div>Телефон: <span class="tel">(817)4822137</span></div><div class="email">rusfortarnoga2@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "Великоустюгское ПАТП"</span><br />

			   <span class="locality"><strong>162390, Вологодская обл, Великоустюгский р-н, г Великий Устюг</strong></span>   

			   <span class="street-address">ул Транспортная, 4</span>

			   <div>Телефон: <span class="tel">(817)3827348</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Смелков Максим Александрович</span><br />

			   <span class="locality"><strong>162480, Вологодская обл, Бабаевский р-н, г Бабаево</strong></span>   

			   <span class="street-address">ул 1 Мая, 76</span>

			   <div>Телефон: <span class="tel">(817)4321379</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Горынцев Виталий Григорьевич</span><br />

			   <span class="locality"><strong>161440, Вологодская обл, Никольский р-н, г Никольск</strong></span>   

			   <span class="street-address">ул Заводская, 15 Ж</span>

			   <div>Телефон: <span class="tel">(81754)21634</span></div><div class="email">gorunzev@list.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "Техосмотр"</span><br />

			   <span class="locality"><strong>162603, Вологодская область, г.Череповец</strong></span>   

			   <span class="street-address">ул. Краснодонцев, 30А</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Строй плюс Авто"</span><br />

			   <span class="locality"><strong>162000, Вологодская обл, Грязовецкий р-н, г Грязовец</strong></span>   

			   <span class="street-address">ул Волкова, 33а</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Фомичев Александр Вениаминович</span><br />

			   <span class="locality"><strong>161100, Вологодская обл, Кирилловский р-н, г Кириллов</strong></span>   

			   <span class="street-address">ул Ленина, 140</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Дэна"</span><br />

			   <span class="locality"><strong>162840, Вологодская обл, Устюженский р-н, г Устюжна</strong></span>   

			   <span class="street-address">ул Гагарина, 38</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
            <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>