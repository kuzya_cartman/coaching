<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Забайкальском крае</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Забайкальском крае. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Забайкальский край, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Забайкальском крае</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Забайкальском крае:</p>
		  
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ИП Базаров Олег Викторович</span><br />

			   <span class="locality"><strong>687000, Забайкальский край, Агинский р-н, пгт Агинское</strong></span>   

			   <span class="street-address">мкр Питомник, 4</span>

			   <div>Телефон: <span class="tel">(302) 3935435</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ОАО "ЧитарегионОПР"</span><br />

			   <span class="locality"><strong>674310, Забайкальский край, Приаргунский р-н, пгт Приаргунск</strong></span>   

			   <span class="street-address">ул Аксенова, 1</span>

			   <div>Телефон: <span class="tel">(3022) 391198</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ОАО "ЧитарегионОПР"</span><br />

			   <span class="locality"><strong>Нерчинский район, г. Нерчинск</strong></span>   

			   <span class="street-address">ул. Лесная, д.7</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ОАО "ЧитарегионОПР"</span><br />

			   <span class="locality"><strong>г. Балей, п. Калга, с. Алек-Завод, с. Кыра.</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>673302, Забайкальский край, Карымский р-н, пгт Карымское</strong></span>   

			   <span class="street-address">ул Медицинская</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>687200, Забайкальский край, Дульдургинский р-н, с Дульдурга</strong></span>   

			   <span class="street-address">ул 50 лет Октября</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>674520, Забайкальский край, Оловяннинский р-н, пгт Ясногорск</strong></span>   

			   <span class="street-address">ул Степная</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>674650, Забайкальский край, Забайкальский р-н, пгт Забайкальск</strong></span>   

			   <span class="street-address">ул Партизанская</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>674050, Забайкальский край, Улетовский р-н, с Улеты</strong></span>   

			   <span class="street-address">ул Школьная</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>674230, Забайкальский край, Акшинский р-н, с Акша</strong></span>   

			   <span class="street-address">ул Партизанская</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>673204, Забайкальский край, Хилокский р-н, г Хилок</strong></span>   

			   <span class="street-address">ул Ленина</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>673403, Забайкальский край, Нерчинский р-н, г Нерчинск</strong></span>   

			   <span class="street-address">ул Советская</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>673500, Забайкальский край, Сретенский р-н, г Сретенск</strong></span>   

			   <span class="street-address">ул Клубная</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АВТО-Лайф"</span><br />

			   <span class="locality"><strong>673630, Забайкальский край, Газимуро-Заводский р-н, с Газимурский Завод</strong></span>   

			   <span class="street-address">ул Красная</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Михайлова Ольга Владимировна</span><br />

			   <span class="locality"><strong>Борзинский район, г. Борзя,</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">9243733353</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Михайлова Ольга Владимировна</span><br />

			   <span class="locality"><strong>Чернышевский район, п. Чернышевск</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Михайлова Ольга Владимировна</span><br />

			   <span class="locality"><strong>Улетовский район, с. Улеты</strong></span>   

			   <span class="street-address"> </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Веселов Виталий Анатольевич</span><br />

			   <span class="locality"><strong>674673, Забайкальский край, Краснокаменский р-н, г Краснокаменск</strong></span>   

			   <span class="street-address">мкр Восточный</span>

			   <div>Телефон: <span class="tel">3024528090</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Кудрявцева Светлана Ивановна</span><br />

			   <span class="locality"><strong>673005, Забайкальский край, Петровск-Забайкальский р-н, г Петровск-Забайкальский</strong></span>   

			   <span class="street-address">ул Спортивная, 28</span>

			   <div>Телефон: <span class="tel">(30236) 31599</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО Фирма "Эридан Восток"</span><br />

			   <span class="locality"><strong>673732, Забайкальский край, Могочинский р-н, г Могоча</strong></span>   

			   <span class="street-address">ул Малокрестьянская, 3</span>

			   <div>Телефон: <span class="tel">(3022)32-21-35, 32-28-29</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО Фирма "Эридан Восток"</span><br />

			   <span class="locality"><strong>674600, Забайкальский край, Борзинский р-н, г Борзя</strong></span>   

			   <span class="street-address">пер Товарный, 20</span>

			   <div>Телефон: <span class="tel">8(3022)32-21-35, 32-28-29</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Цыденжапов Валерий Батоевич</span><br />

			   <span class="locality"><strong>687420, Забайкальский край, Могойтуйский р-н, пгт Могойтуй</strong></span>   

			   <span class="street-address">ул Шукшина, 1 А</span>

			   <div>Телефон: <span class="tel">(30235) 21187</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Мищенко Юрий Владимирович</span><br />

			   <span class="locality"><strong>673302, Забайкальский край, Карымский р-н, пгт Карымское</strong></span>   

			   <span class="street-address"> пер Стадионный, 7</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Турушев Петр Осипович</span><br />

			   <span class="locality"><strong>673014, Забайкальский край, Петровск-Забайкальский р-н, с Малета</strong></span>   

			   <span class="street-address">ул Пионерская, 6</span>

			   <div>Телефон: <span class="tel">(302)3641100</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">КГУП "Автомобильные дороги Забайкалья"</span><br />

			   <span class="locality"><strong>673500, Забайкальский край, Сретенский р-н, г Сретенск</strong></span>   

			   <span class="street-address">ул Карелина, 60</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">КГУП "Автомобильные дороги Забайкалья"</span><br />

			   <span class="locality"><strong>674500, Забайкальский край, Оловяннинский р-н, пгт Оловянная</strong></span>   

			   <span class="street-address">ул Кирпичная, 13</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">КГУП "Автомобильные дороги Забайкалья"</span><br />

			   <span class="locality"><strong>674480, Забайкальский край, Ононский р-н, с Нижний Цасучей</strong></span>   

			   <span class="street-address">ул Коммунальная, 41</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">КГУП "Автомобильные дороги Забайкалья"</span><br />

			   <span class="locality"><strong>673630, Забайкальский край, Газимуро-Заводский р-н, с Газимурский Завод</strong></span>   

			   <span class="street-address">ул Красноармейская, 43</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Михеев Александр Иванович</span><br />

			   <span class="locality"><strong>673204, Забайкальский край, Хилокский р-н, г Хилок</strong></span>   

			   <span class="street-address">ул Декабристов, 46</span>

			   <div>Телефон: <span class="tel">(302)3722984</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Мункуев Барадий Галсанович</span><br />

			   <span class="locality"><strong>687200, Забайкальский край, Дульдургинский р-н, с Дульдурга</strong></span>   

			   <span class="street-address">ул 50 лет Октября, 4 А</span>

			   <div>Телефон: <span class="tel">(924)3788071, (924)2957237</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Мелентьев Василий Николаевич</span><br />

			   <span class="locality"><strong>674650, Забайкальский край, Забайкальский р-н, пгт Забайкальск</strong></span>   

			   <span class="street-address">ул Ононская, 21</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Ильин Владимир Ильич</span><br />

			   <span class="locality"><strong>673370, Забайкальский край, Шилкинский р-н, г Шилка</strong></span>   

			   <span class="street-address">пер Лесной, 4</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Размахнина Галина Демьяновна</span><br />

			   <span class="locality"><strong>673462, Забайкальский край, Чернышевский р-н, пгт Чернышевск</strong></span>   

			   <span class="street-address">ул Лазо, 23а</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Малов Алексей Владимирович</span><br />

			   <span class="locality"><strong>673732, Забайкальский край, Могочинский р-н, г Могоча</strong></span>   

			   <span class="street-address">ул Им В.Малова, 19</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
            <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>