<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Томской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Томской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Томская область, адреса, телефоны"/>
<meta name="classification" content="transportation222"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Томской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Томской области:</p>
			
			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Эсперанто"</span><br />

			   <span class="locality"><strong>636840, Томская обл, Асиновский р-н, г Асино</strong></span>   

			   <span class="street-address">ул им Ленина, 170 кор.2 </span>

			   <div>Телефон: <span class="tel">(38241) 25001</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Орлов Виктор Александрович</span><br />

			   <span class="locality"><strong>636601, Томская обл, Парабельский р-н, с Парабель</strong></span>   

			   <span class="street-address">ул Техническая, 1Г </span>

			   <div>Телефон: <span class="tel">(38252)27686</span></div><div class="email">ORL@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Титова Анна Ивановна</span><br />

			   <span class="locality"><strong>636131, Томская обл, Шегарский р-н, с Мельниково</strong></span>   

			   <span class="street-address">ул Суворова, 1 </span>

			   <div>Телефон: <span class="tel">9039553637</span></div><div class="email">LAZAND@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Титова Анна Ивановна</span><br />

			   <span class="locality"><strong>636160, Томская обл, Кожевниковский р-н, с Кожевниково</strong></span>   

			   <span class="street-address">ул Ленина, 51 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Титова Анна Ивановна</span><br />

			   <span class="locality"><strong>636300, Томская обл, Кривошеинский р-н, с Кривошеино</strong></span>   

			   <span class="street-address">ул Тракторная, 6 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Титова Анна Ивановна</span><br />

			   <span class="locality"><strong>636331, Томская обл, Молчановский р-н, с Молчаново</strong></span>   

			   <span class="street-address">ул Промышленная, 44 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автоэксперт"</span><br />

			   <span class="locality"><strong>636850, Томская обл, Зырянский р-н, с Зырянское</strong></span>   

			   <span class="street-address">ул 60 лет СССР, 9 </span>

			   <div>Телефон: <span class="tel">(903) 9553637</span></div><div class="email">LAZAND@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автоэксперт"</span><br />

			   <span class="locality"><strong>636930, Томская обл, Первомайский р-н, с Первомайское</strong></span>   

			   <span class="street-address">ул Ленинская, 91Б </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автоэксперт"</span><br />

			   <span class="locality"><strong>636200, Томская обл, Бакчарский р-н, с Бакчар</strong></span>   

			   <span class="street-address">ул Восточная, 2 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автоэксперт"</span><br />

			   <span class="locality"><strong>636400, Томская обл, Чаинский р-н, с Подгорное</strong></span>   

			   <span class="street-address">ул 60 лет ВЛКСМ, 42 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "СТК"</span><br />

			   <span class="locality"><strong>636500, Томская обл, Верхнекетский р-н, рп Белый Яр</strong></span>   

			   <span class="street-address">ул Гагарина, 57 </span>

			   <div>Телефон: <span class="tel">(38258) 23583</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Виадук"</span><br />

			   <span class="locality"><strong>636071, Томская обл, г Северск</strong></span>   

			   <span class="street-address">ул Лесная, 13А </span>

			   <div>Телефон: <span class="tel">8 (382-3) 77-93-48</span></div><div class="email">autoservis@sibmail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АТП КБУ №3"</span><br />

			   <span class="locality"><strong>636071, Томская обл, г Северск</strong></span>   

			   <span class="street-address">ул Лесная, 6А </span>

			   <div>Телефон: <span class="tel">8 (382-3) 54-79-40</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ЦТК"</span><br />

			   <span class="locality"><strong>636785, Томская обл, г Стрежевой</strong></span>   

			   <span class="street-address">ул Транспортная, 26 </span>

			   <div>Телефон: <span class="tel">(38259) 68870</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Зырянов Антон Борисович</span><br />

			   <span class="locality"><strong>636460, Томская обл, Колпашевский р-н, г Колпашево</strong></span>   

			   <span class="street-address">ул Сосновая, 1/1 </span>

			   <div>Телефон: <span class="tel">(382-54) 5-21-28</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ОАО "СХК"</span><br />

			   <span class="locality"><strong>Томская обл, г Северск</strong></span>   

			   <span class="street-address">ул Транспортная, 75 </span>

			   <div>Телефон: <span class="tel">(3823) 527931</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "СТД"</span><br />

			   <span class="locality"><strong>636460, Томская обл, Колпашевский р-н, г Колпашево</strong></span>   

			   <span class="street-address">ул Обская, 93 </span>

			   <div>Телефон: <span class="tel">(382-54) 41541</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Клинов Владимир Иванович</span><br />

			   <span class="locality"><strong>636700, Томская обл, Каргасокский р-н, с Каргасок</strong></span>   

			   <span class="street-address">ул Таежная, 17 </span>

			   <div>Телефон: <span class="tel">(38253) 21816</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Диагностика"</span><br />

			   <span class="locality"><strong>636785, Томская обл, г Стрежевой</strong></span>   

			   <span class="street-address">ул Вахская </span>

			   <div>Телефон: <span class="tel">(382) 5956504, (913) 8006122</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Юматов Сергей Владимирович</span><br />

			   <span class="locality"><strong>636760, Томская обл, Александровский р-н, с Александровское</strong></span>   

			   <span class="street-address">пер Северный, 18 </span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
			   <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	  
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
	  
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>