<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Где пройти техосмотр в Владимирской области</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Пройти техосмотр в Владимирской области. Адреса и телефоны пунктов ТО." />
<meta name="keywords" content="где, пройти, техосмотр, Владимирская область, адреса, телефоны"/>
<meta name="classification" content="transportation"/>
<meta name="robots" content="index,follow"/>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/main.css" />
</head>
<body>
<div id="bodywrap">
  <div id="header-shad"></div>
  <div id="wrapper">
    <div id="header"> </div>
    <div id="navigation">
      <?php include("../inc/728X15.inc"); ?>
    </div>
    <div id="container">

      <div id="content">
        <h1>Где пройти техосмотр в Владимирской области</h1>
        <div style="padding: 12px; float: left">
          <?php include("../inc/300X250.inc"); ?>
        </div>
        <div style="margin: 10px">
          <p>Адреса и телефоны пунктов технического осмотра в Владимирской области:</p>
		  

			<div class="vcard">
			<div> <span class="category"><strong>Пункты технического осмотра автомобилей</strong></span> </div>
			<div class="adr">

				<span class="fn org">ООО "Муромское РТП"</span><br />

			   <span class="locality"><strong>602262, Владимирская обл, г Муром</strong></span>   

			   <span class="street-address">ул Стахановская, 10 А</span>

			   <div>Телефон: <span class="tel">8-903-833-11-53</span></div><div class="email">rtp.murom@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Муромское РТП"</span><br />

			   <span class="locality"><strong>602266, Владимирская обл, г Муром</strong></span>   

			   <span class="street-address">ш Владимирское, 23а</span>

			   <div>Телефон: <span class="tel">89107745739</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">МУП "АТП ЗАТО"</span><br />

			   <span class="locality"><strong>600910, Владимирская обл, г Радужный</strong></span>   

			   <span class="street-address"> кв-л 10-й, 6 </span>

			   <div>Телефон: <span class="tel">(49254) 36346</span></div><div class="email">atp_raduga@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Грачева Ольга Сергеевна</span><br />

			   <span class="locality"><strong>Владимирская область, Кольчугинский р-н, п. Зеленоборский</strong></span>   

			   <span class="street-address">д.24а</span>

			   <div>Телефон: <span class="tel">89107728443, 89107747364</span></div><div class="email">gto33@yandex.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Грачева Ольга Сергеевна</span><br />

			   <span class="locality"><strong>601800, Владимирская обл, Юрьев-Польский р-н, г Юрьев-Польский</strong></span>   

			   <span class="street-address">проезд Автотранспортный, 16</span>

			   <div>Телефон: <span class="tel">(49246) 23953</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Вилков Александр Николаевич</span><br />

			   <span class="locality"><strong>601480, Владимирская обл, Гороховецкий р-н, г Гороховец</strong></span>   

			   <span class="street-address">ул Набережная, 2</span>

			   <div>Телефон: <span class="tel">(920)2353335</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Фомин Леонид Моисеевич</span><br />

			   <span class="locality"><strong>602331, Владимирская обл, Селивановский р-н, пгт Красная Горбатка</strong></span>   

			   <span class="street-address">ул Коммунальная, 27 А</span>

			   <div>Телефон: <span class="tel">(910)7705979</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "АЦДА"</span><br />

			   <span class="locality"><strong>601651, Владимирская обл, Александровский р-н, г Александров</strong></span>   

			   <span class="street-address">ул Маяковского, 42</span>

			   <div>Телефон: <span class="tel">(49244) 25046</span></div><div class="email">acda@bk.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Сталкер Групп"</span><br />

			   <span class="locality"><strong>601501, Владимирская обл, г Гусь-Хрустальный</strong></span>   

			   <span class="street-address">ул Кравчинского, 14</span>

			   <div>Телефон: <span class="tel">(4924) 124563,</span></div><div class="email">stalkeravto.33@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ЦентрАвтоДиагностики"</span><br />

			   <span class="locality"><strong>Владимирская обл, г Ковров</strong></span>   

			   <span class="street-address">ул Еловая, 1в</span>

			   <div>Телефон: <span class="tel">(4923) 238466</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Авто Техцентр"</span><br />

			   <span class="locality"><strong>601655, Владимирская обл, Александровский р-н, г Александров</strong></span>   

			   <span class="street-address">проезд Южный, 15</span>

			   <div>Телефон: <span class="tel">(49244) 98610</span></div><div class="email">sevasevaseva@inbox.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "САС"</span><br />

			   <span class="locality"><strong>601442, Владимирская обл, Вязниковский р-н, г Вязники</strong></span>   

			   <span class="street-address">ул Вокзальная, 26</span>

			   <div>Телефон: <span class="tel">(49233) 39599</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "САС"</span><br />

			   <span class="locality"><strong>601524, Владимирская обл, Гусь-Хрустальный р-н, д Красный Якорь</strong></span>   

			   <span class="street-address">ул Рабочая, 12</span>

			   <div>Телефон: <span class="tel">(49241) 32684</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "САС"</span><br />

			   <span class="locality"><strong>601301, Владимирская обл, Камешковский р-н, г Камешково</strong></span>   

			   <span class="street-address">ул Коруновой, 2</span>

			   <div>Телефон: <span class="tel">(492) 4822732</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "САС"</span><br />

			   <span class="locality"><strong>602103, Владимирская обл, Меленковский р-н, г Меленки</strong></span>   

			   <span class="street-address">ул Мира, 15</span>

			   <div>Телефон: <span class="tel">(432) 4724346</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Автоконтроль-33"</span><br />

			   <span class="locality"><strong>601122, Владимирская обл, Петушинский р-н, г Покров</strong></span>   

			   <span class="street-address">ул Октябрьская, 111</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Степанов Владимир Васильевич</span><br />

			   <span class="locality"><strong>601351, Владимирская обл, Судогодский р-н, г Судогда</strong></span>   

			   <span class="street-address">ул Коммунистическая, 1</span>

			   <div>Телефон: <span class="tel">49235-2-35-00</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО Сервис+"</span><br />

			   <span class="locality"><strong>601570, Владимирская обл, Гусь-Хрустальный р-н, г Курлово</strong></span>   

			   <span class="street-address">ул Заводская, 16А</span>

			   <div>Телефон: <span class="tel">(492) 4155027</span></div><div class="email">nolesvlaid@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ВРТС"</span><br />

			   <span class="locality"><strong>601110, Владимирская обл, Петушинский р-н, г Костерево</strong></span>   

			   <span class="street-address">ул Трансформаторная, 1Б</span>

			   <div>Телефон: <span class="tel">(4922)432457</span></div><div class="email">ark_vldizel@vtsnet.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ВРТС"</span><br />

			   <span class="locality"><strong>601293, Владимирская обл, Суздальский р-н, г Суздаль</strong></span>   

			   <span class="street-address">ул Промышленная, 1</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "СпецАвтоЦентр"</span><br />

			   <span class="locality"><strong>601012, Владимирская обл, Киржачский р-н, г Киржач</strong></span>   

			   <span class="street-address">ул Садовая, 1</span>

			   <div>Телефон: <span class="tel">(49237) 21662</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Маслова Наталья Витальевна</span><br />

			   <span class="locality"><strong>601800, Владимирская обл, Юрьев-Польский р-н, г Юрьев-Польский</strong></span>   

			   <span class="street-address">ул Чехова, 3 А</span>

			   <div>Телефон: <span class="tel">(49246)238-07</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ТехКонтроль+"</span><br />

			   <span class="locality"><strong>601507, Владимирская обл, г Гусь-Хрустальный</strong></span>   

			   <span class="street-address">ул Курловская</span>

			   <div>Телефон: <span class="tel">8-49241-3-56-59</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ТехКонтроль+"</span><br />

			   <span class="locality"><strong>601550, Владимирская область, Гусь хрустальный</strong></span>   

			   <span class="street-address">ул. Курловская, 22 Л.</span>

			   <div>Телефон: <span class="tel">(961) 2595979</span></div><div class="email">techcontrol33@mail.ru</div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ТехАвто"</span><br />

			   <span class="locality"><strong>601012, Владимирская обл, Киржачский р-н, г Киржач</strong></span>   

			   <span class="street-address">ул Садовая, 1</span>

			   <div>Телефон: <span class="tel">(49237)21622</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Сидорков Владимир Васильевич</span><br />

			   <span class="locality"><strong>601293, Владимирская обл, Суздальский р-н, г Суздаль</strong></span>   

			   <span class="street-address">ул Транспортная, 5</span>

			   <div>Телефон: <span class="tel">(910)7738862</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Кедр"</span><br />

			   <span class="locality"><strong>Владимирская обл, Киржачский р-н, г Киржач</strong></span>   

			   <span class="street-address">ул Шелковиков, 27 б</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "Техконтроль"</span><br />

			   <span class="locality"><strong>601204, Владимирская обл, Собинский р-н, г Собинка</strong></span>   

			   <span class="street-address">ул Садовая, 3</span>

			   <div>Телефон: <span class="tel">4924224869</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ЗАО "Техконтроль"</span><br />

			   <span class="locality"><strong>601204, Владимирская обл, Собинский р-н, г Собинка</strong></span>   

			   <span class="street-address">ул Садовая, 5</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>


			<div class="vcard">

			<div class="adr">

				<span class="fn org">ООО "ТрансСервисГрупп"</span><br />

			   <span class="locality"><strong>601784, Владимирская обл, Кольчугинский р-н, г Кольчугино</strong></span>   

			   <span class="street-address">ул Луговая, 14б</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>

			</div>

			<div class="vcard">

			<div class="adr">

				<span class="fn org">ИП Корноухов Сергей Владимирович</span><br />

			   <span class="locality"><strong>601110, Владимирская обл, Петушинский р-н, г Костерево</strong></span>   

			   <span class="street-address">ул Трансформаторная, 3</span>

			   <div>Телефон: <span class="tel">нет информации</span></div></div>
            <br />
            <div>Время работы <span class="workhours">Вторник с 9:00 до 20:00 <br />
              Среда с 9:00 до 18:00 <br />
              Четверг с 9:00 до 18:00 <br />
              Пятница с 9:00 до 18:00<br />
              Обеденный перерыв с 14:00 до 15:00 <br />
              Суббота с 9:00 до 14:00<br />
              Воскресенье, понедельник - выходной.</span> </div>
			</div>

			

          <?php include("../inc/relative2.php"); ?>
        </div>
        <div style="padding: 12px; float: center">
          <?php include("../inc/468X60.inc"); ?>
        </div>
      </div>
	        <div id="leftside">
        <div id="leftcolumn">
          <p>
          <center>
            <?php include("../inc/googlesearch.inc"); ?>
          </center>
          </p>
        </div>
        <div id="leftcolumn">
          <?php include("../inc/menu.inc"); ?>
        </div>
      </div>
      <div class="clear"></div>
      <div id="content-bottom">
        <?php include("../inc/content-bottom.inc"); ?>
      </div>
    </div>
    <div id="footer">
      <?php include("../inc/footer.inc"); ?>
    </div>
  </div>
  <div id="footer-shad"></div>
</div>
<?php include("../inc/tracking.inc"); ?>
</body>
</html>